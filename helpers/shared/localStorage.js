import cookie from "cookie";

export default function getServerLocalStorage(context) {

  console.log(process.server, 'server checking in local storage');

  if (process.server) {

    console.log(context.req, 'req');

    if (context.req) {

      if (context.req.headers.cookie) {

        const parsedCookies = cookie.parse(context.req.headers.cookie);

        if (parsedCookies['x-allpasal-access-token']) {

          return parsedCookies['x-allpasal-access-token'];

        } else {

          return null;

        }

      } else {

        return null;

      }

    } else {

      return null;

    }

  }

}
