export default function errorHandler(error, context) {
  switch (error.response.data.code) {
    case 500:
      handle500Error()
      break;
    case 422:
      handle422Error()
      break;
    case 503:
      handle503Error()
      break;
    case 400:
      handleOtherError()
      break;
    case 401:
      handleOtherError()
      break;
    case 403:
      handleOtherError()
      break;
    default:
      handleOtherError()
  }
  function handle500Error() {
    context.$store.commit("error/SET_422_ERROR", false);
    context.$store.commit("error/SET_422_ERROR_MESSAGE", null);
    context.$store.commit('notification/COMMIT_SNACKBAR', {
      message: error.response.data.message,
      color: 'red'
    });
  }
  function handle422Error() {
    context.$store.commit("error/SET_422_ERROR", true);
    context.$store.commit("error/SET_422_ERROR_MESSAGE", error.response.data.data);
  }
  function handleOtherError() {
    context.$store.commit("error/SET_422_ERROR", false);
    context.$store.commit("error/SET_422_ERROR_MESSAGE", null);
    context.$store.commit('notification/COMMIT_SNACKBAR', {
      message: error.response.data.message,
      color: 'red'
    });
  }
  function handle503Error() {
    context.$store.commit("error/SET_422_ERROR", false);
    context.$store.commit("error/SET_422_ERROR", false);
    context.$store.commit("error/SET_MAINTENANCE_CODE", true);
  }
}
