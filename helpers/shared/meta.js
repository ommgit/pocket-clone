
function updateMetaTags(metaTagsObject){
  let payload = []
  Object.keys(metaTagsObject).forEach((items) => {

    if(items === 'property'){
      let a= {}
      for(const item in metaTagsObject[items]){
        a[item] = metaTagsObject[items][item]
      }
      payload.push(a)
    }else{
      let b = {}
      for(const item in metaTagsObject[items]){
        b[item] = metaTagsObject[items][item]
      }
      payload.push(b)
    }

  })
  return payload
}

export default {
  updateMetaTags
}
