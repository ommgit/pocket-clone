const registerRoutes = [
  {
    name: 'StoreRegistration',
    path: '/store/register/:id/:type',
    component: 'pages/store/registration/StoreRegistration'
  },
  {
    name: 'JoinNow',
    path: '/store/join-now',
    component: 'pages/store/registration/JoinNow'
  },
  {
    name: 'PreOrderDetail',
    path: '/store/preorder/:product_slug',
    component: 'pages/store/preorder/detail'
  }
];

export default registerRoutes;
