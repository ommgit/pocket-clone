import extendedAllRoutes from "./router/index";

export default {

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    htmlAttrs: {
      lang: "en"
    },
    bodyAttrs: {
      class: ["my-style"]
    },
    meta: [{
      charset: "utf-8",
    }],
    script: [
      {
        hid: 'stripe',
        src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBkShwqbN4_vK84kDHYqGU1PC4Cm9M-zgM&libraries=places',
        defer: true
      }
    ],

    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'https://staging.testpasal.com/uploads/general-setting/jawxb9x2hm_1645530617.png'
      },
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900'},
      {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css'},
    ]
  },

  router: {

    extendRoutes(routes, resolve) {

      for (let i = 0; i < extendedAllRoutes.length; i++) {

        routes.push({
          name: extendedAllRoutes[i]['name'],
          path: extendedAllRoutes[i]['path'],
          component: resolve(__dirname, extendedAllRoutes[i]['component'])
        })

      }
    },

    middleware: ['shared/maintenanceMode', 'locale/i18n']

  },

  middleware: ['shared/authenticated', 'shared/noVisitIfAuthenticated'],

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/app.css',
    '@/assets/scss/app.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~/plugins/vuex-persist.client', ssr: false},
    {src: '~/plugins/axios'},
    {src: '~/plugins/i18n'},
    {src: '~/plugins/registerErrorComponents', ssr: false},
    {src: '~/plugins/truncate', ssr: false},
    {src: '~/plugins/zoomer', ssr: false},
    {src: '~/plugins/swiper', mode: "client"},
    {src: '~/plugins/vueScrollTo', ssr: false},
    {src: '~/plugins/timer', ssr: false},
    {src: '~/plugins/editor', ssr: false},
    {src: '~/plugins/pic-zoom', ssr: false},
    {src: '~/plugins/pusher', ssr: false},
    {src: '~/plugins/multiTabState.client.js', mode: 'client'},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/dotenv',
    ['nuxt-fontawesome', {
      component: 'fa', //customize component name
      imports: [{
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['faBookmark']
      },
        {
          set: '@fortawesome/free-brands-svg-icons',
          icons: ['faGithub']
        },
        {
          set: '@fortawesome/free-regular-svg-icons',
          icons: ['faBookmark',
            'faTrashAlt',
            'faStar',
            'faThumbsUp',
            'faStickyNote',
            'faFileVideo',
            'faAddressCard',
            'faFolder',
            'faTimesCircle',
            'faFlushed',
            'faFileAlt',
            'faSave',
            'faShareSquare',
            'faCopy',
            'faEye',
            'faEyeSlash',
            'faPlusSquare',
            'faUser',
            'faUserCircle',
            'faEnvelope',
            'faAddressBook',
            'faKissWinkHeart',
          ]
        },
      ]
    }]

  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-mq',
    '@nuxtjs/axios',
    'vue2-editor/nuxt',
    '@nuxtjs/dayjs',
    '@nuxtjs/toast'
  ],

  toast: {
    position: 'bottom-right',
    duration:1000,
    register: [ // Register custom toasts

    ]
  },

  'mq': {
    defaultBreakpoint: 'sm',
    breakpoints: {
      mobile: 576,
      tablet: 768,
      ipad: 992,
      laptop: 1200,
      desktop: 1600,
      xlscreen: Infinity
    }

  },

  axios: {
    baseURL: process.env.BASE_URL,
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          darkBlue: '#003574',
          darkOrange: '#FF5C00',
          logInIconBackground: '#e9e6e6',
          logInTextFieldBorder: '#003574',
        },
        dark: {
          darkBlue: '#FF5C00',
          darkOrange: '#003574',
        }
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: ['vue-i18n'],
  },

  server: {
    host: "0.0.0.0"
  }
}
