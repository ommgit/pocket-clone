import notification from './notification';
import category from './category';
import product from './product';
import tableConfig from "./table-configuration";
import settings from "./settings";
import pusherNotification from "./pusher-notification"
let sharedVuexState = {
  notification,
  category,
  product,
  tableConfig,
  settings,
  pusherNotification
}
export default {
  sharedVuexState
}
