import PusherNotificationService from '@/services/shared/pusher-notification';
import {getField, updateField} from "vuex-map-fields";

const pusherNotificationModule = {

    namespaced: true,

    state: {},

    getters: {

        getField,
    
      },
    
      mutations: {
    
        updateField,
    
        COMMIT_RESPONSE() {
        }
    
      },

      actions: {

        getStoreUserNotification({commit}) {
    
          return new Promise((resolve, reject) => {
    
            return PusherNotificationService.getStoreUserNotification().then(response => {
    
              commit("COMMIT_RESPONSE");
    
              resolve(response);
    
            }).catch(error => {
    
              reject(error);
    
            });
    
          });
    
        },
    
      },
}

export default pusherNotificationModule;