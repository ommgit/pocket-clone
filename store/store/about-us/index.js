import service from '@/services/store/about-us'

const state = {

}
const getters  ={

}


const mutations = {

}

const actions = {
  getAboutUs({commit}){
    return new Promise((resolve,reject) =>{
      service.aboutUs().then(res =>{
        commit('ABOUT_US');
        resolve(res)
      }).catch((err)=>{
        reject(err)
      })
    })
  }

}

export  default  {
  namespaced:true,
  state,
  getters,
  mutations,
  actions
}
