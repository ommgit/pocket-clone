import Service from "@/services/store/investment";

const state = {
  businessPlanDetailInfo: '',
  isFromBusinessPlan: false,
  amount: {
    start_range: 0,
    end_range: 0
  },
  fromOfflineInvestmentOrNot: false
}

const mutations = {
  SET_BUSINESS_PLAN_INFORMATION(state, payload){
    state.businessPlanDetailInfo = payload
  },
  SET_WHETHER_FROM_INVESTMENT_OR_ONT(state, payload){
    state.fromOfflineInvestmentOrNot = payload
  },
  SET_IS_FROM_BUSINESS_PLAN(state, payload) {
    state.isFromBusinessPlan = payload;
  },
  SET_BUSINESS_PLAN_AMOUNT(state, payload) {
    state.amount = payload;
  },
  COMMIT_MESSAGE() {
  }
}

const getters = {
  businessPlanDetailInformation(state) {
    return state.businessPlanDetailInfo;
  },
  fromOfflineInvestmentOrNot(state){
    return state.fromOfflineInvestmentOrNot;
  },
  GET_IS_FROM_BUSINESS_PLAN(state) {
    return state.isFromBusinessPlan;
  },
  GET_BUSINESS_PLAN_AMOUNT(state) {
    return state.amount;
  }
}

const actions = {
  changeUserPassword(_, payload) {
    return Service.changeUserPassword(payload);
  },
  getBusinessPlans() {
    return Service.getBusinessPlans();
  },
  getBusinessPlanDetail(_, payload) {
    return Service.getBusinessPlanDetail(payload);
  },
  submitIPSConnectForm(_, payload) {
    return Service.submitIPSConnectForm(payload);
  },
  getIPSConnectTransactionDetail(_, payload) {
    return Service.getIPSConnectTransactionDetail(payload);
  },
  getMyBusinessPlans() {
    return Service.getMyBusinessPlans();
  },
  getMyBusinessPlansInvestmentReturns(_, payload) {
    return Service.getMyBusinessPlansInvestmentReturns(payload);
  },
  getCurrentUserWalletDetail({commit}) {
    return new Promise((resolve, reject) => {
      return Service.getCurrentUserWalletDetail().then(res => {
        resolve(res.data);
        commit('COMMIT_MESSAGE')
      }).catch(err => {
        reject(err);
      });
    });
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
