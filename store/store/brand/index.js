import service from '@/services/store/brand'

const brand_module = {
  namespaced: true,
  state : {

  },
  getters : {

  },
  mutations : {
    SET_BRAND_PRODUCT(){

    },
    FOLLOW_BRAND(){}
  },
  actions : {
    getBrandDetail({commit}, payload) {
      return new Promise((resolve, reject) => {
        service.brandDetail(payload).then(response =>{
          commit('SET_BRAND_PRODUCT');
          resolve(response);
        }).catch(err => {
          reject(err)
        })
      })
    },
    getBrandProduct({commit}, payload){
      return new Promise((resolve, reject) => {
        service.getBrandProduct(payload).then(response =>{
          commit('SET_BRAND_PRODUCT');
          resolve(response);
        }).catch(err => {
          reject(err)
        })
      })
    },
    followBrand({commit}, code){
      return new Promise((resolve, reject) => {
        service.followBrands(code).then(response =>{
          commit('FOLLOW_BRAND');
          resolve(response);
        }).catch(err => {
          reject(err)
        })
      })
    },
    getFeaturedBrand({commit}){
      return new Promise((resolve, reject) => {
        service.getFeaturedBrand().then(response =>{
          commit('FOLLOW_BRAND');
          resolve(response);
        }).catch(err => {
          reject(err)
        })
      })
    }

  }
}







export default brand_module;
