import Service from "@/services/store/withdraw";
import helperFunctions from "@/helpers/shared/generatePageNumber";

const state = {
  list_of_withdraw:null,
  loading:false
}
const getters = {
  GET_WITHDRAW_LIST:(state)=>{
    return state.list_of_withdraw
  }
}
const mutations = {
  COMMIT_MESSAGE() {

  },
  SET_WITHDRAW_LIST(state, payload){
    state.list_of_withdraw = payload
  },
  SET_CURRENT_PAGE(state, payload) {
    state.list_of_withdraw.meta.current_page = payload;
  },
  SET_LOADER_FOR_TABLE(state, payload) {
    state.loading = payload;
  },
  PUSH_WITHDRAW_REQUEST(state, payload){
    helperFunctions.dataTableSerialNumberGenerator(  state.list_of_withdraw.data.unshift(payload));
  }
}

const actions = {
  submitWithdraw({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.submitWithdraw(payload).then(res => {
        commit('PUSH_WITHDRAW_REQUEST', res.data)
        resolve(res);
      }).catch(err => {
        reject(err);
      });
    });
  },
  getBanks({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getBanks(payload).then(res => {
        resolve(res.data);
        commit('COMMIT_MESSAGE')
      }).catch(err => {
        reject(err);
      });
    });
  },
  getListOfWithdrawRequests({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getListOfWithdrawRequests(payload).then(res => {
        helperFunctions.dataTableSerialNumberGenerator(res.data);
        commit('SET_WITHDRAW_LIST', res)
      }).catch(err => {
        reject(err);
      });
    });
  },
  getBalanceWithdrawRequestDetail({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getBalanceWithdrawRequestDetail(payload).then(res => {
        resolve(res.data);
        commit('COMMIT_MESSAGE')
      }).catch(err => {
        reject(err);
      });
    });
  },
  getBalanceWithdrawRequestVerificationDetails({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getBalanceWithdrawRequestVerificationDetails(payload).then(res => {
        resolve(res.data);
        commit('COMMIT_MESSAGE')
      }).catch(err => {
        reject(err);
      });
    });
  },
  cancelWithdrawRequest(_, payload) {
    return Service.cancelWithdrawRequest(payload);
  },
  paginatedLists({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getListOfWithdrawRequests(payload).then(response => {
        helperFunctions.dataTableSerialNumberGenerator(response.data);
        commit('SET_WITHDRAW_LIST', response)
        resolve(response)
      }).catch(error => {
        reject(error);
      });

    });

  },
}

export default {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
};
