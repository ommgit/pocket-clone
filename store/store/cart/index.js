import CartService from "@/services/store/cart";

const state = {

  cart_count: 0,
  checkoutList: [],
  placeOrderSuccessfulMessage: null,
  total_cart_item: [],
  eligible_products: [],

}

const mutations = {

  COMMIT_MESSAGE() {
  },

  PRODUCT_CART_COUNT(state, payload) {


    state.cart_count = payload.total_carts;

  },

  SET_CART_PRODUCTS(state, payload) {

    state.cart = payload;

  },

  SET_CHECKOUT_LIST(state, payload) {

    state.checkoutList = payload;

  },

  PLACE_ORDER_SUCCESSFUL_MESSAGE(state, payload) {

    state.placeOrderSuccessfulMessage = payload;

  },

  SET_PRODUCT_CART(state, payload) {
    state.cart_items = [...state.cart_items, payload]
  },

  REMOVE_CHECKOUT(state, payload) {
    console.log(payload, 'cart place order');
    state.checkoutList = state.checkoutList.filter((item) => item.cart_code !== payload)
  },

  SET_ELIGIBLE_PRODUCTS(state, payload) {
    state.eligible_products = payload;
  },

  REMOVE_ITEM_FROM_CART(state, code) {
    let cartProductIndex = state.eligible_products.findIndex(data => data['cart_code'] === code);
    state.eligible_products.splice(cartProductIndex, 1);
  },

  REMOVE_ALL_CART_ITEMS(state, payload) {
    state['eligible_products'] = payload;
  }

}

const getters = {

  GET_CARTS_COUNT: state => {

    return state.cart_count;

  },

  GET_ELIGIBLE_PRODUCTS: state => {

    return state.eligible_products;

  },

  GET_PLACE_ORDER_PRODUCTS: state => {

    return state.checkoutList;

  },

  GET_ORDER_PLACE_SUCCESSFUL_MESSAGE: state => {

    return state.placeOrderSuccessfulMessage;

  },

}

const actions = {

  addProductToCart({commit}, payload) {

    return new Promise((resolve, reject) => {

      return CartService.addProductToStoreCart(payload).then(response => {

        response.data.product_packaging_types.map(data => {

          if (data.package_code === response.data.package_code) {

            data.color = '#FF5C00';

          } else {

            data.color = '#c3c3c3';

          }

        })
        commit("SET_PRODUCT_CART", response.data);

        resolve(response);


      }).catch(error => {

        reject(error);

      });

    });

  },

  loadAllCartProductsCount({commit, payload}) {

    return new Promise((resolve, reject) => {

      CartService.getAllCartsProductsCount(payload).then(response => {

        commit("PRODUCT_CART_COUNT", response.data);

        resolve(response);

      }).catch(error => {

        reject(error);

      });

    });

  },

  loadAllCartProducts({commit}) {

    return new Promise((resolve, reject) => {

      return CartService.loadAllCartProduct().then(response => {

        response.data.map(data => {

          data.increaseLoadingButton = false;

          data.decreaseLoadingButton = false;

          data.is_checked = false;

          if (data['stock'] === 0) {

            if (data['quantity'] === 1) {

              data.disableDecreaseButton = true;

              data.disableIncreaseButton = true;

            } else if (data['quantity'] > 1) {

              data.disableIncreaseButton = true;

              data.disableDecreaseButton = false;

            }

          } else if (data['stock'] !== 0) {

            if (data['quantity'] === 1) {

              data.disableDecreaseButton = true;

              data.disableIncreaseButton = false;

            } else if (data['quantity'] > 1) {

              data.disableDecreaseButton = false;

              data.disableIncreaseButton = false;

            }
          }

        });

        commit("COMMIT_MESSAGE");

        resolve(response);

      }).catch(error => {

        reject(error);

      });

    });

  },

  updateQuantityForProduct({commit, getters}, payload) {

    return new Promise((resolve, reject) => {

      return CartService.updateProductQuantity(payload).then(response => {

        console.log(payload, getters.GET_ELIGIBLE_PRODUCTS, 'response');

        for (let i = 0; i < response.data.length; i++) {

          let responseCartItems = response['data'][i];

          console.log(responseCartItems, 'response');

          for (let j = 0; j < getters.GET_ELIGIBLE_PRODUCTS.length; j++) {

            let cartItems = getters.GET_ELIGIBLE_PRODUCTS[j];

            if (responseCartItems["cart_code"] === cartItems["cart_code"]) {

              cartItems["display_stock"] = responseCartItems["display_stock"];

              cartItems["stock"] = responseCartItems["stock"];

              if (cartItems["quantity"] === 1) {
                cartItems["disableIncreaseButton"] = false;
              } else if (cartItems["quantity"] > 1) {
                cartItems["disableDecreaseButton"] = false;

                cartItems["disableIncreaseButton"] = false;
              }

              if (cartItems["stock"] === 0) {
                cartItems["disableIncreaseButton"] = true;

                cartItems["disableDecreaseButton"] = false;
              } else {

                cartItems["disableIncreaseButton"] = false;
              }

              cartItems["increaseLoadingButton"] = false;

              cartItems["decreaseLoadingButton"] = false;

            }

            if (payload['cart_code'] === cartItems['cart_code']) {

              cartItems['quantity'] = payload['quantity'];

            }

          }
        }

        commit("COMMIT_MESSAGE");

        resolve(response);

      }).catch(error => {

        reject(error);

      });

    });

  },

  placeOrder({commit}, payload) {

    return new Promise((resolve, reject) => {

      return CartService.placeStoreOrder(payload).then(response => {

        // commit("COMMIT_MESSAGE");
        commit("SET_CHECKOUT_LIST", {})

        resolve(response);

      }).catch(error => {

        reject(error);

      });

    });

  },

  addProductToPreOrder({commit}, payload) {
    return new Promise((resolve, reject) => {
      return CartService.addPreOrder(payload).then(response => {
        resolve(response);
        commit("ADD_PRODUCT_TO_CART");
      }).catch(error => {
        reject(error);
      });
    });
  },

  updateStoreDetailsFromPlaceorder({commit}, payload) {
    return new Promise((resolve, reject) => {
      return CartService.updateStoreDetailsFromPlaceorder(payload).then(response => {
        resolve(response);
        commit("UPDATE_STORE_DETAIL");
      }).catch(error => {
        reject(error);
      });
    })
  }

}

export default {

  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
