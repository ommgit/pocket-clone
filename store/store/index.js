import login from './auth';
import cart from './cart';
import dashboard from './dashboard';
import storeOrder from './store-order';
import payment from './offline-payment';
import preOrder from './store-pre-order'
import verification from './account-verification';
import investment from './investment';
import quiz from './quiz-game';
import visionMission from './vision-mission';
import aboutUs from './about-us';
import navigation from './preorder-category';
import brand from './brand'
let storeVuexState = {

  login,
  cart,
  aboutUs,
  payment,
  dashboard,
  storeOrder,
  preOrder,
  verification,
  investment,
  quiz,
  brand,
  visionMission,
  navigation
}

export default {

  storeVuexState

}
