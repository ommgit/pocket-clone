import {getField, updateField} from "vuex-map-fields";
import service from "@/services/store/store-pre-order";
import helperFunctions from "@/helpers/shared/generatePageNumber";

const storePreOrderModule = {
    namespaced: true,
    state: {
        warehouse_code: '',
        selected_pre_order: null,
        pre_order_products: [],
        pre_order_categories: [],
        pre_order_end_time: '',
        all_pre_orders: [],
        tab_changed: 0,
        preOrderDetail: null,
        preOrderDetailProducts: [],
        preOrderListFilter: {
            store_preorder_code: '',
            start_time: '',
            end_time: '',
            payment_status: '',
            price_condition: '',
            total_price: '',
            page: '',
        },
        preOrderProductFilter: {
            product_name: '',
            date_from: '',
            data_to: '',
            is_active: '',
            page: '',
        },
        preOrderProductSelectedToHasBeenOrdered: '',
        product_detail: null,
        relatedProducts: [],
        preOrderLists: null,
        isLoading: false,
        isFromTab: false,
        fromPreOrderDetailPage:false
    },
    getters: {
        getField,

        GET_SELECTED_PRODUCT_THAT_IS_ORDERED_INSTANTLY: state => {
            return state.preOrderProductSelectedToHasBeenOrdered;
        },


        GET_FROM_DETAIL_PAGE:state =>{
          return state.fromPreOrderDetailPage
        },

        PREORDER_PRODUCT_FILTER: state => {
            return state.preOrderProductFilter;
        },

        PREORDER_PRODUCT_NAME: state => {
            return state.preOrderProductFilter.product_name;
        },

        PREORDER_PRODUCT_PAGE: state => {
            return state.preOrderProductFilter.page;
        },

        PREORDER_PRODUCT_DATE_TO: state => {
            return state.preOrderProductFilter.data_to;
        },

        PREORDER_PRODUCT_DATE_FROM: state => {
            return state.preOrderProductFilter.date_from;
        },

        PREORDER_PRODUCT_IS_ACTIVE: state => {
            return state.preOrderProductFilter.is_active;
        },

        STORE_PREORDER_CODE: state => {
            return state.preOrderListFilter.store_preorder_code;
        },
        PREORDER_START_TIME: state => {
            return state.preOrderListFilter.start_time;
        },
        PREORDER_END_TIME: state => {
            return state.preOrderListFilter.end_time;
        },
        PREORDER_PAYMENT_TYPE: state => {
            return state.preOrderListFilter.payment_status;
        },
        PREORDER_PRICE_CONDITION: state => {
            return state.preOrderListFilter.price_condition;
        },
        PREORDER_TOTAL_PRICE: state => {
            return state.preOrderListFilter.total_price;
        },
        PREORDER_PAGE: state => {
            return state.preOrderListFilter.page;
        },

        PRE_ORDER_FILTER: state => {
            return state.preOrderListFilter;
        },

        PRE_ORDER_END_TIME: state => {
            return state.pre_order_end_time;
        },
        SELECTED_WAREHOUSE_CODE_FOR_PRE_ORDER: state => {
            return state.warehouse_code
        },
        PRE_ORDER: state => {
            return state.selected_pre_order
        },
        PRE_ORDER_PRODUCTS: state => {
            return state.pre_order_products;
        },
        PRE_ORDER_CATEGORIES: state => {
            return state.pre_order_categories;
        },
        IS_TAB_CHANGED: state => {
            return state.tab_changed;
        },
        ALL_PRE_ORDERS_LIST: state => {
            return state.all_pre_orders;
        },
        PRE_ORDER_DETAIL_PRODUCTS: state => {
            return state.preOrderDetailProducts
        },
        PRE_ORDER_ALL_DETAILS: state => {
            return state.preOrderDetail
        },
        PRODUCT_DETAIL: state => {
            return state.product_detail;
        },
        GET_PRE_ORDER_RELATED_PRODUCTS: state => {
            return state.relatedProducts;
        },
        GET_FROM_TAB: state => {
            return state.isFromTab;
        }
    },
    mutations: {
        updateField,

        IS_FROM_TAB(state, payload) {
            state.isFromTab = payload;
        },

        SET_LOADER_FOR_TABLE(state, payload) {
            state.isLoading = payload;
        },

        SET_CURRENT_PAGE(state, payload) {
            state.preOrderLists.meta.current_page = payload;
        },
        SET_DROP_DOWN_VALUE(state, payload){
          state.fromPreOrderDetailPage = payload
        },

        SET_PRE_ORDER_LIST(state, payload) {
            state.preOrderLists = payload;
        },

        SELECTED_PRODUCT_THAT_IS_ORDERED_INSTANTLY(state, payload) {
            state.preOrderProductSelectedToHasBeenOrdered = payload;
        },

        SET_PREORDER_PRODUCT_NAME(state, payload) {
            state.preOrderProductFilter.product_name = payload;
        },
        SET_PREORDER_PRODUCT_DATE_TO(state, payload) {
            state.preOrderProductFilter.data_to = payload;
        },
        SET_PREORDER_PRODUCT_DATE_FROM(state, payload) {
            state.preOrderProductFilter.date_from = payload;
        },
        SET_PREORDER_PRODUCT_IS_ACTIVE(state, payload) {
            state.preOrderProductFilter.is_active = payload;
        },

        SET_PREORDER_PRODUCT_PAGE(state, payload) {
            state.preOrderProductFilter.page = payload;
        },

        SET_STORE_PREORDER_CODE(state, payload) {
            state.preOrderListFilter.store_preorder_code = payload;
        },
        SET_PREORDER_START_TIME(state, payload) {
            state.preOrderListFilter.start_time = payload;
        },
        SET_PREORDER_END_TIME(state, payload) {
            state.preOrderListFilter.end_time = payload;
        },
        SET_PREORDER_PAYMENT_TYPE(state, payload) {
            state.preOrderListFilter.payment_status = payload;
        },
        SET_PREORDER_PRICE_CONDITION(state, payload) {
            state.preOrderListFilter.price_condition = payload;
        },
        SET_PREORDER_TOTAL_PRICE(state, payload) {
            state.preOrderListFilter.total_price = payload;
        },
        SET_PREORDER_PAGE(state, payload) {
            state.preOrderListFilter.page = payload;
        },

        SET_SELECTED_WAREHOUSE_FOR_PRE_ORDER(state, payload) {
            state.warehouse_code = payload;
        },
        SET_END_TIME(state, payload) {
            state.pre_order_end_time = payload;
        },
        SET_PRE_ORDER_CATEGORIES(state, payload) {
            state.pre_order_categories = payload;
        },
        VIEW_WAREHOUSE_PRE_ORDER() {
        },
        SELECTED_PRE_ORDER(state, payload) {
            state.selected_pre_order = payload;
        },
        PRE_ORDER_PRODUCTS(state, payload) {
            state.pre_order_products = payload;
        },
        ALL_PRE_ORDERS_LIST(state, payload) {
            state.all_pre_orders = payload;
        },
        TAB_CHANGED(state, payload) {
            state.tab_changed = payload;
        },
        LOAD_PRE_ORDER_PRODUCTS(state, payload) {
            state.preOrderDetail = payload;
            state.preOrderDetailProducts = payload.details;
        },
        DELETE_PRODUCT_FROM_PRE_ORDER_LIST(state, payload) {
            let selectedProduct = state.preOrderDetailProducts.filter(data => data.warehouse_preorder_product_code !== payload.data.warehouse_preorder_product_code);
            state.preOrderDetailProducts = selectedProduct;
        },
        PRE_ORDER_PRODUCT(state, payload) {
            state.preOrderDetailProducts = payload;
        },
        ADD_ALL_PRE_ORDER(state, payload) {
            state.preOrderLists = payload;
        },
        LOAD_RELATED_PRODUCTS() {
        },
        LOAD_PRE_ORDER_PRODUCT_DETAIL(state, detail) {
            state.product_detail = detail;
        },
        SET_PRE_ORDER_RELATED_PRODUCTS(state, payload) {
            state.relatedProducts = payload;
        },
    },
    actions: {

        loadPreOrderProductDetail({commit}, payload) {
            return new Promise((resolve, reject) => {
                service.getPreOrderProductDetail(payload).then(response => {
                    let productDetail = response.data;
                    productDetail['first_variant'].map(data => {
                            data.color = '#c3c3c3';
                    });
                    if (response.data.product_packaging_types.length > 0) {
                        response.data.product_packaging_types.map(data => {
                            data.color = '#c3c3c3';
                        });
                    }
                    commit("LOAD_PRE_ORDER_PRODUCT_DETAIL", productDetail);
                    resolve(productDetail);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        paginatedLists({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.loadPreOrdersListByPagination(payload).then(response => {
                    helperFunctions.dataTableSerialNumberGenerator(response.data);
                    response.data.map(preOrder => {

                        preOrder.total_price >= preOrder.total_individual_target ? preOrder.is_individual_target_acquired = true : preOrder.is_individual_target_acquired = false;

                        preOrder.total_group_order >= preOrder.total_group_target ? preOrder.is_group_target_acquired = true : preOrder.is_group_target_acquired = false;

                    });
                    commit("ADD_ALL_PRE_ORDER", response);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        LoadAllRelatedProducts({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.preOrderRelatedProducts(payload).then(response => {
                  commit('SET_PRE_ORDER_RELATED_PRODUCTS', response.data);
                  resolve(response.data);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        /* filterPreOrderList({commit}, payload) {
             return new Promise((resolve, reject) => {
                 return service.loadPreOrderFilterList(payload).then(response => {
                     helperFunctions.dataTableSerialNumberGenerator(response.data.data);
                     commit("VIEW_WAREHOUSE_PRE_ORDER")
                     resolve(response.data);
                 }).catch(error => {
                     reject(error);
                 });
             });
         },*/

        filterPreOrderProductList({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.loadPreOrderProductFilterList(payload).then(response => {
                    helperFunctions.dataTableSerialNumberGenerator(response.data.details);
                    commit("PRE_ORDER_PRODUCT", response.data.details);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        preOrdersDate({commit}) {
            return new Promise((resolve, reject) => {
                return service.loadPreOrderListDates().then(response => {
                    commit("VIEW_WAREHOUSE_PRE_ORDER")
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        preOrdersList({commit}) {
            return new Promise((resolve, reject) => {
                return service.loadPreOrdersList().then(response => {
                    helperFunctions.dataTableSerialNumberGenerator(response.data);
                    response.data.map(preOrder => {

                        preOrder.total_price >= preOrder.total_individual_target ? preOrder.is_individual_target_acquired = true : preOrder.is_individual_target_acquired = false;

                        preOrder.total_group_order >= preOrder.total_group_target ? preOrder.is_group_target_acquired = true : preOrder.is_group_target_acquired = false;

                    });
                    commit("ADD_ALL_PRE_ORDER", response);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        getPreOrderDetails({commit}, pre_order_code) {


            return new Promise((resolve, reject) => {
                return service.getPreOrderProductDetail(pre_order_code).then(response => {
                    commit("LOAD_PRE_ORDER_PRODUCTS", response.data);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        loadProductsAccordingToQueryString({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.loadPreOrderDateProductsList(payload).then(response => {
                    commit("VIEW_WAREHOUSE_PRE_ORDER")
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        removeProductFromPreOrderList({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.removeFromPreOrderList(payload).then(response => {
                    commit("DELETE_PRODUCT_FROM_PRE_ORDER_LIST", response);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        updatePreOrderProductQuantity({commit}, payload) {
            return new Promise((resolve, reject) => {
                return service.updatePreOrderQuantity(payload).then(response => {
                    commit("VIEW_WAREHOUSE_PRE_ORDER");
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
      getPreOrderListViewDetails({commit}, payload) {
        return new Promise((resolve, reject) => {
          return service.getPreOrderListViewDetails(payload).then(response => {
            resolve(response);
            commit("ADD_PRODUCT_TO_CART");
          }).catch(error => {
            reject(error);
          });
        });
      },
      getNextLevelVariants({commit}, payload) {
        return new Promise((resolve, reject) => {
          return service.getNextLevelVariantsInfo(payload).then(response => {
            if (response.data.length > 0) {
              response.data.map(data => data.color = '#c3c3c3');
            }
            commit("ADD_PRODUCT_TO_CART");
            resolve(response);
          }).catch(error => {
            reject(error);
          });
        });
      },
      getPreOrderVariantInfo({commit}, payload) {
        return new Promise((resolve, reject) => {
          service.getPreOrderVariantDetail(payload).then(response => {
            if (response.data.product_packaging_types.length > 0) {
              response.data.product_packaging_types.map(data => {
                data.color = '#c3c3c3';
              });
            }
            commit("lOAD_VARIANT_DETAIL");
            resolve(response);
          }).catch(error => {
            reject(error);
          });
        });
      },
      loadPreOrderDetails({commit}, pre_order_code) {

        return new Promise((resolve, reject) => {
          return service.loadPreOrderDetails(pre_order_code).then(response => {
            commit("LOAD_PRE_ORDER_PRODUCTS", response.data);
            resolve(response);
          }).catch(error => {
            reject(error);
          });
        });
      },
    }
};
export default storePreOrderModule;
