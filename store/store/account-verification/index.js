import Service from "../../../services/store/account-verification";

const state = {
  fromStoreProfile:false
}

const mutations = {

  COMMIT_MESSAGE() {
  },
  SET_FROM_STORE_PROFILE:(state,payload)=>{
    state.fromStoreProfile = payload
  }

}

const getters = {
  FROM_STORE_PROFILE:(state)=>{
    return state.fromStoreProfile
  }
}

const actions = {

  generateOtpByPhone({commit}) {

    return new Promise((resolve, reject) => {

      Service.generateOtpByPhone().then((response) => {

        commit("COMMIT_MESSAGE", response.data);

        resolve(response.data);

      }).catch(error => {

        reject(error);

      })

    });

  },

  generateOtpByEmail({commit}) {

    return new Promise((resolve, reject) => {

      Service.generateOtpByEmail().then((response) => {

        commit("COMMIT_MESSAGE", response.data);

        resolve(response.data);

      }).catch(error => {

        reject(error);

      })

    });

  },

  verifyByPhone({commit}, payload) {

    return new Promise((resolve, reject) => {

      Service.verifyByPhone(payload).then((response) => {

        commit("COMMIT_MESSAGE", response.data);

        resolve(response.data);

      }).catch(error => {

        reject(error);

      })

    });

  },

  verifyByEmail({commit}, payload) {

    return new Promise((resolve, reject) => {

      Service.verifyByEmail(payload).then((response) => {

        commit("COMMIT_MESSAGE", response.data);

        resolve(response.data);

      }).catch(error => {

        reject(error);

      })

    });

  },
  verifyWhileCreatingStore({commit}, payload){
    return new Promise((resolve, reject) => {

      Service.verifyWhileCreatingStore(payload).then((response) => {

        commit("COMMIT_MESSAGE", response.data);

        resolve(response);

      }).catch(error => {

        reject(error);

      })

    });
  },
  generateOtpWhileCreatingStore({commit}, payload){
    return new Promise((resolve, reject) => {

      Service.generateOtpWhileCreatingStore(payload).then((response) => {


        commit("COMMIT_MESSAGE");

        resolve(response.data);

      }).catch(error => {

        reject(error);

      })

    });
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
