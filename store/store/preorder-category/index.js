import service from "@/services/store/preorder-category";
import {getField, updateField} from "vuex-map-fields";

const navigationModule = {
  namespaced: true,
  state: {
    allLevelCategories: [],
    addProductLoader: false,
    categoryIndex: null
  },
  getters: {
    getField,
    ADD_PRODUCT_LOADING: state => {
      return state.addProductLoader;
    },
    ALL_LEVEL_CATEGORIES: state => {
      return state.allLevelCategories;
    },
    GET_CATEGORY_INDEX(state) {
      return state.categoryIndex;
    }
  },
  mutations: {
    updateField,
    SET_ALL_LEVEL_CATEGORIES(state, payload) {
      state.allLevelCategories = payload;
    },
    ADD_PRODUCT_LOADER(state, payload) {
      state.addProductLoader = payload;
    },
    COMMIT_CATEGORY_INDEX(state, payload) {
      state.categoryIndex = payload;
    }
  },
  actions: {
    loadTreeStructureCategoriesForHomePageNavigation({commit}) {
      return new Promise((resolve, reject) => {
        service.getAllCategories().then((response) => {
          commit('SET_ALL_LEVEL_CATEGORIES', response.data.data);
          resolve(response.data.data);
        }).catch(error => {
          reject(error);
        });
      });
    }
  },
};
export default navigationModule;
