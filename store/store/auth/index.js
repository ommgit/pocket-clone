import authService from '@/services/store/auth'
import * as Cookies from 'js-cookie'
import {getField, updateField} from "vuex-map-fields";

const authModule = {

  namespaced: true,

  state: {
    user_credential: null,
    isLoggedIn: false,
    storeCred: null,
    userDetails: null,
    selectedVerificationType: null,
    associatedWarehouse: [],
    store_connected_warehouse: [],
    drawerState: null,
  },

  getters: {

    getField,

    GET_NAVIGATION_DRAWER_STATE(state) {
      return state.drawerState;
    },

    GET_ASSOCOCIATED_WAREHOUSE: state => {
      return state.associatedWarehouse;
    },

    TOKENCRED: state => {
      if (state.user_credential) {
        return state.user_credential['tokens'];
      } else {
        return null;
      }
    },

    ISLOGINED: state => {
      return state.isLoggedIn;
    },

    ROLEID: state => {
      if (state.user_credential) {
        return state.user_credential['user']['user_type'];
      } else {
        return null;
      }
    },

    USERCRED: state => {
      if (state.user_credential) {
        return state.user_credential['user'];
      } else {
        return null;
      }
    },

    IS_BILLABLE: state => {
      if (state.user_credential) {
        return state.user_credential['store_details']['is_billable'];
      } else {
        return false;
      }
    },

    ACCOUNT_STATUS: state => {
      if (state.user_credential) {
        return state.user_credential['account_status'];
      } else {
        return null;
      }
    },

    STORE_WAREHOUSE: state => {
      return state.store_connected_warehouse;
      // if (state.user_credential) {
      //   return state.user_credential['connected_warehouses'];
      // } else {
      //   return null;
      // }
    },
    STORECRED: state => {
      return state.storeCred;
    },
    GET_SELECTED_VERIFICATION_TYPE: state => {
      return state.selectedVerificationType;
    },
    GET_USER_DETAILS: state => {
      return state.userDetails;
    },

  },

  mutations: {

    updateField,

    SET_IS_BILLABLE(state, payload) {
      state.user_credential['store_details']['is_billable'] = payload;
    },

    SET_NAVIGATION_DRAWER_STATE(state, drawer) {
      state.drawerState = drawer;
    },

    SET_ASSOCIATED_WAREHOUSE(state, payload) {
      state.associatedWarehouse = payload;
    },

    LOGIN_USER(state, payload) {
      if (payload instanceof Object) {
        state.user_credential = payload;
      } else {
        if (payload === 'phone') {
          state.user_credential['user']['user_credential'] = 1
        } else {
          state.user_credential['user']['is_email_verified'] = 1
        }
      }


    },

    SET_ISLOGINED(state, payload) {

      state.isLoggedIn = payload;

    },

    LOGOUT(state) {
      localStorage.removeItem("vuex");
      localStorage.clear();
      state.user_credential = null;
      state.isLoggedIn = false;
    },

    COMMIT_REQUEST() {
    },
    SET_STORE_CRED(state, payload) {
      state.storeCred = payload;
    },
    SET_SELECTED_VERIFICATION_TYPE(state, payload) {
      state.selectedVerificationType = payload;
    },
    SET_USER_DETAILS(state, payload) {
      state.userDetails = payload;
    },
    SET_STORE_WAREHOUSE(state, data) {
      state.store_connected_warehouse = data;
    },
    SET_REFRESH_TOKEN(state, data) {
      state.user_credential.token.access_token = data.access_token;
      state.user_credential.token.refresh_token = data.refresh_token;
    },

  },

  actions: {

    login({commit}, payload) {

      return new Promise((resolve, reject) => {

        authService.login(payload).then((response) => {

          commit("LOGIN_USER", response.data);

          Cookies.set('x-allpasal-access-token', response.data.tokens.access_token, {expires: 1});

          Cookies.set('x-allpasal-refresh-token', response.data.tokens.refresh_token, {expires: 3});

          resolve(response.data);

        }).catch(error => {

          reject(error);

        })

      });

    },

    sendResetPasswordLink({commit}, payload) {

      return new Promise((resolve, reject) => {

        authService.sendResetPasswordLinkToEmail(payload).then((response) => {

          commit("COMMIT_REQUEST");

          resolve(response);

        }).catch(error => {

          reject(error);

        });

      });

    },

    logout({commit}, context) {

      return new Promise((resolve, reject) => {

        authService.logout(context).then((response) => {

          commit("LOGOUT");

          Cookies.remove('x-allpasal-access-token');

          Cookies.remove('x-allpasal-refresh-token');

          resolve(response);

        }).catch(error => {

          reject(error);

        });

      });

    },

    resetUserPassword({commit}, payload) {

      return new Promise((resolve, reject) => {

        authService.resetPassword(payload).then((response) => {

          resolve(response);

          commit("COMMIT_REQUEST");

        }).catch(error => {

          reject(error);

        })

      });

    },

    refreshToken({commit}, refreshToken) {

      return new Promise((resolve, reject) => {

        authService.refreshAccessToken(refreshToken).then(response => {

          // commit("SET_REFRESH_TOKEN", response.data);

          Cookies.set('x-allpasal-access-token', response.access_token, {expires: 1});

          Cookies.set('x-allpasal-refresh-token', response.refresh_token, {expires: 3});

          resolve(response);

        }).catch(error => {

          reject(error);

        });

      });


    },

    updateStoreDetailsFromPlaceorder({commit}, payload) {
      return new Promise((resolve, reject) => {
        return authService.updateStoreDetailsFromPlaceorder(payload).then(response => {
          resolve(response);
          commit("COMMIT_REQUEST");
        }).catch(error => {
          reject(error);
        });
      })
    }

  },

}

export default authModule;
