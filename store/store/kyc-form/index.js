import {getField, updateField} from "vuex-map-fields";
import service from "@/services/store/kyc-form"

const kycmodule = {
  namespaced: true,
  state: {},
  getters: {
    getField,
  },
  mutations: {
    updateField,
    KYC_FORM() {},
    UPLOAD_VIDEO(){},
  },
  actions: {
    saveKYCForm({commit}, payload) {
      return new Promise((resolve, reject) => {
        return service.saveKYC(payload).then(response => {
          resolve(response.data);
          commit("KYC_FORM");
        }).catch(error => {
          reject(error);
        });
      });
    },

    loadKYCFormData() {
      return new Promise((resolve, reject) => {
        return service.KYCFormData().then(response => {
          resolve(response.data);
        }).catch(error => {
          reject(error);
        });
      });
    },

    uploadKYCVideo({commit}, payload) {
      return new Promise((resolve, reject) => {
        return service.uploadVideoForAgreement(payload).then(response => {
          commit("UPLOAD_VIDEO");
          resolve(response);
        }).catch(error => {
          reject(error);
        });
      });
    },

    loadAgreementVideos(){
      return new Promise((resolve, reject) => {
        return service.loadKYCAgreement().then(response => {
          resolve(response);
        }).catch(error => {
          reject(error);
        });
      });
    }
  },
};

export default kycmodule;
