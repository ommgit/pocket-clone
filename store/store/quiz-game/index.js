import service from '@/services/store/quiz-game'


const state = {
  participator_detail:null,
  submission_detail:[],
  passage_detail:[]
}

const getters = {
  GET_PARTICIPATOR_DETAIL:(state)=>{
    return state.participator_detail;
  },
  GET_SUBMISSION_DETAIL:(state) => {
    return state.submission_detail;
  },
  GET_PASSAGE_DETAIL:(state) =>{
    return state.passage_detail
  }
}

const mutations = {
  PARTICIPATION_FORM(){
  },
  SET_PARTICIPATOR_DETAIL(state,payload){
    state.participator_detail = payload
  },
  UPDATE_BTN_STATUS(state, {question_code}){
    state.passage_detail['passage_questions_details'].map((item)=>{
      if(item['question_code'] === question_code){
        item['real_answer'] = ''
      }
    })
  },
  SET_SUBMISSION_DETAIL(state, payload){
    state.submission_detail = payload
  },
  SET_PASSAGE_DETAIL(state, payload){
    state.passage_detail = payload
  },
  SET_QUIZZES_DETAIL(){}

}


const actions = {
  submitParticipationForm({commit},payload){
    return new Promise((resolve, reject) =>{
      service.formSubmit(payload).then(res =>{
        commit('PARTICIPATION_FORM')
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  getParticipatorDetail({commit}){
    return new Promise((resolve, reject) => {
      service.getQuizDetail().then(res=> {
        if(res.data['quiz_passage_detail']){
          res.data["quiz_passage_detail"]['passage_questions_details'].map((item)=>{
            item['answer'] = "";
            item['selected_option'] = "";
            item['real_answer'] = "";
          })
        }
        commit('SET_PARTICIPATOR_DETAIL', res.data['quiz_participator_detail']);
        commit('SET_PASSAGE_DETAIL', res.data['quiz_passage_detail'])
        if(res.data['quiz_submission_detail']){
          commit('SET_SUBMISSION_DETAIL', res.data['quiz_submission_detail'])
        }else{
          commit('SET_SUBMISSION_DETAIL', null)
        }
        resolve(res)

      }).catch((err) => {
        reject(err)
      })
    })
  },
  answerFormSubmit({commit, state}, payload){
    return new Promise((resolve, reject) =>{
      service.answerSubmit(payload).then(res =>{
        if(res.data.action){
          Object.entries(res.data['right_answers']).forEach(([key, value])=>{
            // let object = state.passage_detail['passage_questions_details'].find(item=> item['question_code'] === key);
            // object['real_answer'] = value
            state.passage_detail['passage_questions_details'].forEach((item)=>{
              if(item['question_code'] === key){
                item['real_answer'] = value
              }
            })
          });
          commit('SET_PASSAGE_DETAIL', state.passage_detail);
          state.passage_detail['passage_questions_details'].map(item => {
            if(item['real_answer'] == '') {
              item['real_answer'] = item['selected_option']
            }
          });
          commit('SET_PASSAGE_DETAIL', state.passage_detail);
        }else{
          state.passage_detail = null;
          commit('SET_SUBMISSION_DETAIL', res.data);

        }

        resolve(res);
      }).catch((err) => {
        reject(err)
      })
    })
  },
  updateParticipatorDetails({commit}, payload){
    return new Promise((resolve, reject) =>{
      service.updateParticipatorDetail(payload).then(res =>{
        commit('SET_PARTICIPATOR_DETAIL', res.data.data)
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  getQuizzesDetails({commit}){
    return new Promise((resolve, reject) => {
      service.viewPastQuizzesDetail().then(res => {
        commit('SET_QUIZZES_DETAIL')
        resolve(res)
      }).catch((err)=>{
        reject(err)
      })
    })
  },
  getSubmittedQuizzesDetails({commit}, payload){
    return new Promise((resolve, reject) => {
      service.getSubmittedQuizzesDetail(payload).then(res => {
        commit('SET_QUIZZES_DETAIL')
        resolve(res)
      }).catch((err)=>{
        reject(err)
      })
    })
  }

}


export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutations
}
