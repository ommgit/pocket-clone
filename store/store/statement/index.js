import Service from "@/services/store/statement";
import helperFunctions from "@/helpers/shared/generatePageNumber";
const state = {
  list_of_statement:null,
  loading: false,
}

const getters = {
  GET_STATEMENT_LIST:(state)=>{
    return state.list_of_statement
  }
}

const mutations = {
  COMMIT_MESSAGE() {

  },
  SET_STATEMENT_LIST(state, payload){
    state.list_of_statement = payload
  },
  SET_CURRENT_PAGE(state, payload) {
    state.list_of_statement.meta.current_page = payload;
  },
  SET_LOADER_FOR_TABLE(state, payload) {
    state.loading = payload;
  },
}

const actions = {
  getBalanceStatements({commit}, payload) {
    return new Promise((resolve, reject) => {
      return Service.getBalanceStatement(payload).then(res=>{
        helperFunctions.dataTableSerialNumberGenerator(res.data);
        commit('SET_STATEMENT_LIST', res)
      }).catch((err)=>{
        reject(err)
      })
    })

  },
  getTransactionTypes(_, payload) {
    return Service.getTransactionType(payload);
  },
  getStoreCurrentBalance({commit}) {
    return new Promise((resolve, reject) => {
      return Service.getStoreCurrentBalance().then(res => {
        resolve(res);
        commit('COMMIT_MESSAGE')
      }).catch(err => {
        reject(err);
      });
    });
  },
  paginatedLists({commit}, payload) {

    return new Promise((resolve, reject) => {

      return Service.getBalanceStatement(payload).then(response => {

        helperFunctions.dataTableSerialNumberGenerator(response.data);

        commit("SET_STATEMENT_LIST", response)

        resolve(response    );

      }).catch(error => {

        reject(error);

      });

    });

  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
