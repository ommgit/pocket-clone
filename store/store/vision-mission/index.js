import service from '@/services/shared/vision-mission'

const state = {

}
const getters  ={

}


const mutations = {

}

const actions = {
  getCompanyVisionMission({commit}){
    return new Promise((resolve,reject) =>{
      service.getCompanyVisionMission().then(res =>{
        commit('VISION_MISSION');
        resolve(res)
      }).catch((err)=>{
        reject(err)
      })
    })
  },
  getCompanyTimeLine({commit}){
    return new Promise((resolve,reject) =>{
      service.getCompanyTimeLine().then(res =>{
        commit('VISION_MISSION');
        resolve(res)
      }).catch((err)=>{
        reject(err)
      })
    })
  }

}

export  default  {
  namespaced:true,
  state,
  getters,
  mutations,
  actions
}
