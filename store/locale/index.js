const localeModule = {

  namespaced: true,

  state: {

    locales: [
      {
        code: 'en',
        name: 'English'
      },
      {
        code: 'np',
        name: 'नेपाली'
      },
      {
        code: 'ind',
        name: 'हिन्दी'
      },
    ],

    locale: 'en'

  },

  mutations: {

    SET_LANG(state, locale) {

      if (state.locales.find(el => el.code === locale)) {

        state.locale = locale

      }

    }

  }

}

export default {

  locale: localeModule

}
