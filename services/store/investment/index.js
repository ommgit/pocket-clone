import {Api} from "~/services/Api";

function changeUserPassword(data) {
  const url = '/user/change-password';
  return Api.post(url, data);
}

function getBusinessPlans() {
  const url = '/investment-plan';
  return Api.get(url);
}

function getBusinessPlanDetail(code) {
  const url = `/investment-plan/details/${code}`;
  return Api.get(url);
}

function submitIPSConnectForm(data) {
  const url = '/investment-plan/online-pay/subscribe';
  return Api.post(url, data);
}

function getIPSConnectTransactionDetail(code) {
  const url = `/store/connect-ips/validate-store-payment/${code}`;
  return Api.get(url);
}

function getMyBusinessPlans() {
  const url = '/investment-plan/my-subscription';
  return Api.get(url);
}

function getMyBusinessPlansInvestmentReturns(data) {
  const url = `/investment-plan/calculator/${data['code']}/${data['amount']}`;
  return Api.get(url);
}

export default {
  changeUserPassword,
  getBusinessPlans,
  getBusinessPlanDetail,
  submitIPSConnectForm,
  getIPSConnectTransactionDetail,
  getMyBusinessPlans,
  getMyBusinessPlansInvestmentReturns
}
