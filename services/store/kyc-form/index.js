import {Api} from '@/services/Api';


function getSanchalakStatus() {
  const url = '/individual-kyc/sanchalak';

  return Api.get(url);
}

function submitSanchalakForm(data) {
  const url = '/individual-kyc/store';


  return Api.post(url, data);
}

function getAkhtiyarStatus() {
  const url = '/individual-kyc/akhtiyari';

  return Api.get(url);
}

function submitAkhtiyarForm(data) {
  const url = '/individual-kyc/store';

  return Api.post(url, data);
}

function getSamjhautaPatra() {
  const url = '/generate/agreement-paper/samjhauta';

  return Api.get(url);
}

function getAkhtiyariPatra() {
  const url = '/generate/agreement-paper/akhtiyari';

  return Api.get(url);
}

function getAllBankList() {
  const url = '/banks';
  return Api.get(url);
}

function saveKYC(payload) {
  const url = '/firm-kyc/store';
  return Api.post(url, payload);
}

function KYCFormData() {
  const url = '/firm-kyc';
  return Api.get(url);
}

function loadKYCAgreement() {
  const url = '/kyc/agreement-video';
  return Api.get(url);
}

function getAllWalletList() {
  const url = '/digital-wallets';

  return Api.get(url);
}

function getAllRemitsList() {
  const url = '/remits';


  return Api.get(url);
}

function uploadVideoForAgreement(payload) {
  const url = '/submit/agreement-video';

  return Api.post(url, payload);
}

export default {
  getAkhtiyarStatus,
  getAllBankList,
  saveKYC,
  KYCFormData,
  uploadVideoForAgreement,
  loadKYCAgreement,
  getAllWalletList,
  getAllRemitsList
}
