import {Api} from "@/services/Api";

function getStoreTypeForRegistration() {

  const url = '/store-types';

  return Api.get(url);

}

function getStorePackageTypes(storePackageCode) {

  const url = `/store-type-packages/get-packages/${storePackageCode}`;

  return Api.get(url);
}

function registerStore(data) {

  const url = '/store-registration';

  return Api.post(url, data);
}


function sendPhoneOtpAccountVerification(payload){
  const url  = '/account-verification/send-phone-otp-code'
  return Api.post(url, payload)
}

function sendEmailOtpAccountVerification(payload){
  const url  = '/account-verification/send-email-otp-code'
  return Api.post(url, payload)
}
function phoneNumberAlreadyExistOrNot(payload){
  const url  = '/check-phone-exists'
  return Api.get(url, payload)
}
function emailAlreadyExistOrNot(payload){
  const url  = '/check-email-exists'
  return Api.get(url, payload)
}

export default {

  getStoreTypeForRegistration,
  getStorePackageTypes,
  registerStore,
  sendPhoneOtpAccountVerification,
  sendEmailOtpAccountVerification,
  phoneNumberAlreadyExistOrNot,
  emailAlreadyExistOrNot
}
