import {Api} from "@/services/Api";


function getBrandProduct({params, data}) {

  const url = `/brand-product/${params}`;

  return Api.get(url, data);
}

function followBrands(payload) {

  const url = `/brand-follow/${payload}`;

  return Api.get(url);
}


function  brandDetail(slug){
  const  url = '/brand-detail/' + slug
  return Api.get(url);
}


function getFeaturedBrand(){
  const  url = '/featured-brands'
  return Api.get(url);
}





export default {
  followBrands,
  brandDetail,
  getBrandProduct,
  getFeaturedBrand

}
