import {Api} from "@/services/Api";

function generateOtpByPhone() {

  const url = '/store/generate/phone/otp';

  return Api.post(url);
}

function generateOtpByEmail() {

  const url = '/store/generate/email/otp';

  return Api.post(url);
}

function verifyByPhone(payload) {

  const url = '/store/verify/phone/otp';

  return Api.post(url, payload);
}

function verifyByEmail(payload) {

  const url = '/store/verify/email/otp';

  return Api.post(url, payload);
}

function  verifyWhileCreatingStore(payload){
  const url = '/user/account-verification/otp/verify';

  return Api.post(url, payload);
}
function  generateOtpWhileCreatingStore(payload){
  const url = '/user/account-verification/otp/send';

  return Api.post(url, payload);
}




export default {
  generateOtpByPhone,
  generateOtpByEmail,
  verifyByPhone,
  verifyByEmail,
  verifyWhileCreatingStore,
  generateOtpWhileCreatingStore

}
