import {Api} from "@/services/Api";

function loadPreOrderList(warehouse_code) {
    const url = '/store/pre-orders/' + warehouse_code;
    return Api.get(url);
}

function getPreOrderAmountTotal() {
    const url = '/store/pre-order/amounts';
    return Api.get(url);
}

function loadPreOrdersList() {
    const url = '/store/pre-orders';
    return Api.get(url);
}

function loadPreOrdersListByPagination(query) {
    const url = '/store/pre-orders';
    return Api.get(url,query);
}

function loadPreOrderDetails(pre_order_code) {

    const url = '/store/pre-orders/' + pre_order_code + '/details';
    return Api.get(url);
}

function loadPreOrderListDates() {

    const url = '/store/warehouse-pre-orders-date';
    return Api.get(url);

}

function removeFromPreOrderList(payload) {

    const url = '/store/delete/pre-orders/' + payload.store_pre_order_code + '/product/' + payload.warehouse_pre_order_product_code;
    return Api.delete(url,{});

}

function updatePreOrderQuantity(payload) {

    const url = '/store/update/pre-orders/details/' + payload.store_preorder_detail_code;
    return Api.post(url, payload.detail);

}

function loadPreOrderDateProductsList(payload) {

    let url = '/store/warehouse-pre-orders/' + payload.pre_order_code + '/products';
    return Api.get(url, payload.query)

}

function loadPreOrderFilterList(payload) {

    const url = '/store/pre-orders';



    return Api.get(url, payload)

}

function loadPreOrderProductFilterList(payload) {

    const url = '/store/pre-orders/' + payload.pre_order_code + '/details';
    return Api.get(url, payload.filterResponse)

}

function preOrderRelatedProducts(payload) {

    const url = '/pre-orders/'+payload.wh_listing_code+'/related-products/'+payload.product_code;
    return Api.get(url);


}

function getPreOrderProductDetail(payload) {

    let url = '/product/' + payload.slug + '/pre-order/'+payload.warehouse_listing_code;

    return Api.get(url);
}
function getPreOrderListViewDetails(data){
  const url = `/pre-orders/${data['warehouse_preorder_listing_code']}/products/${data['product_code']}/list-view-details`;


  return Api.get(url);
}

function getNextLevelVariantsInfo(payload) {

  const url = '/pre-orders/variant-associations/pre-order-listing-code/' + payload.wh_listing_code + '/product-code/' + payload.product_code + '/variant-value-code/' + payload.variant_value_code + '/variant-depth/' + payload.variant_depth + '/' + payload.ancestor_code;

  return Api.get(url);

}
function getPreOrderVariantDetail(payload) {
  const url = '/product/' + payload.product_slug + '/variant/' + payload.variant_name + '/pre-order/' + payload.wh_listing_code;
  return Api.get(url);
}

export default {
    loadPreOrderList,
    loadPreOrderListDates,
    loadPreOrderDateProductsList,
    loadPreOrdersList,
    loadPreOrderDetails,
    removeFromPreOrderList,
    updatePreOrderQuantity,
    loadPreOrderFilterList,
    loadPreOrderProductFilterList,
    preOrderRelatedProducts,
    getPreOrderAmountTotal,
    loadPreOrdersListByPagination,
    getPreOrderProductDetail,
    getPreOrderListViewDetails,
    getNextLevelVariantsInfo,
   getPreOrderVariantDetail
}
