import {Api} from "@/services/Api";


function  formSubmit(payload){
  const url = `/quiz/participator-detail/store`
  return Api.post(url, payload)
}


function  getQuizDetail(){
  const url = `/quiz/all-detail`
  return Api.get(url)
}



function  answerSubmit(payload){
  const url = `/quiz/submit-detail/store`
  return Api.post(url, payload)
}



function  updateParticipatorDetail({params, data}){
  const url = `/quiz/participator-detail/update/${params}`
  return Api.post(url,data)
}


function  viewPastQuizzesDetail(){
  const url = `/quiz/submissions`

  return Api.get(url)
}


function  getSubmittedQuizzesDetail(submission_code){
  const url = `/quiz/submitted-quiz-detail/${submission_code}`

  return Api.get(url)
}



export default {
  formSubmit,
  getQuizDetail,
  answerSubmit,
  viewPastQuizzesDetail,
  getSubmittedQuizzesDetail,
  updateParticipatorDetail
}
