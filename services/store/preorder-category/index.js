import {Api} from '@/services/Api';

function getAllCategories() {
  const url = '/category-tree';


  return Api.get(url);

}



export default {

  getAllCategories,

};
