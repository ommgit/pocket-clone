import {Api} from "@/services/Api";

function getOurTeams() {
  return Api.get('/our-teams')
}

export default {
  getOurTeams
}
