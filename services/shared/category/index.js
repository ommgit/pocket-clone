import {Api} from "@/services/Api";

function getSearchProducts(payload) {

  const url = '/search-product' + '?keyword=' + payload;

  return Api.get(url);

}

function getAllCategories() {

  const url = '/category-tree';

  return Api.get(url);

}

function loadSelectedCategoryProduct(payload) {

  let url = '/products/categories';

  return Api.get(url, payload)

}
function getParentLevelCategories(payload) {
  const url = '/categories/' + payload + '/upper-category-with-siblings';


  return Api.get(url);
}

function getLowerCategoriesOfSelectedCategory(payload) {
  const url = `/categories/slug/${payload}/lower-categories`;


  return Api.get(url);
}

function getNexPageSearchResult(payload) {
  const url = '/search-product' + '?keyword=' + payload.name + '&page=' + payload.page;
      return Api.get(url);

}


export default {

  getSearchProducts,
  getAllCategories,
  getParentLevelCategories,
  loadSelectedCategoryProduct,
  getNexPageSearchResult,
  getLowerCategoriesOfSelectedCategory

}
