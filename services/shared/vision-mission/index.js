import {Api} from "@/services/Api";

function getCompanyVisionMission() {
  return Api.get('/vision-mission')
}
function  getCompanyTimeLine(){
  return  Api.get('/company-timeline')
}

export default {
  getCompanyTimeLine,
  getCompanyVisionMission
}
