import {Api} from "@/services/Api";

function getProductCollectionLists() {

  const url = '/product-collections';

  return Api.get(url);

}

function warehouseProductCollection() {

  const url = '/warehouse/warehouse-product-collections';

  return Api.get(url);

}

function selectedWarehouseProductCollection(payload) {

  const url = `/warehouse/warehouse-product-collection-details/products/${payload['slug']}?page=${payload['page']}`;

  return Api.get(url);

}

function selectedWarehouseProductCollectionDetails(payload) {

  const url = `/warehouse/warehouse-product-collection-details/${payload['slug']}?page=${payload['page']}`;

  return Api.get(url);

}


function selectedProductCollectionAllProductsList(payload) {

  const url = `products/product-collection/${payload['slug']}?page=${payload['page']}`;

  return Api.get(url);

}

function selectedProductCollectionDetails(payload) {

  const url = `product-collection/${payload['slug']}`;

  return Api.get(url);

}




export default {

  getProductCollectionLists,
  selectedProductCollectionAllProductsList,
  warehouseProductCollection,
  selectedWarehouseProductCollection,
  selectedWarehouseProductCollectionDetails,
  selectedProductCollectionDetails

}
