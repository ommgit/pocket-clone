import {Api} from "@/services/Api";

function getStoreUserNotification() {
    let url = 'store-user/notifications'

    return Api.get(url);
}

export default {
    getStoreUserNotification
}