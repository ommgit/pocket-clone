import Vue from 'vue';
import PicZoom from "../utils/vue-piczoom";

Vue.use(PicZoom);

Vue.component('pic-zoom', PicZoom);
