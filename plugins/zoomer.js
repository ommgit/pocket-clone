import Vue from 'vue'
import imageZoom from 'vue-image-zoomer';

Vue.use(imageZoom);

Vue.component('image-zoom', imageZoom);
