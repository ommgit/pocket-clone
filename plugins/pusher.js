import Vue from 'vue';
import * as Cookies from 'js-cookie'

const token = Cookies.get('x-allpasal-access-token');
Vue.use(require('vue-pusher'), {
    api_key: '9a104d47d64f496aaccc',
    options: {
        cluster: 'ap2',
        forceTLS: true,
        encrypted: true,
        authEndpoint: process.env.BASE_URL+'/broadcasting/auth',
        auth: {
        headers: {
            'Authorization': `Bearer ${token}`,
        }
    },
      
    }
});
