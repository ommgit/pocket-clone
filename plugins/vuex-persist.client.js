import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {

  createPersistedState({

    key: 'allpasal_local',

    paths: ['category', 'error', 'locale', 'cart', 'preOrder','login', 'investment']

  })(store)

}
