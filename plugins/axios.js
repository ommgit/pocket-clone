import {setAxios} from "@/utils/Api";
import * as Cookies from 'js-cookie'


export default function ({store, redirect, $axios}) {

  setAxios($axios);

  $axios.onRequest(() => {

    if (store.state.login['isLoggedIn']) {

      $axios.setHeader('Authorization', 'Bearer ' + Cookies.get('x-allpasal-access-token'));

    }

  })

  $axios.onError(error => {

    console.log(error.response, 'error');

    if (error.code) {

      if (error.code === 400) {

        redirect('/error');

      }

    }

    if (error.response) {

      if (error.response.status) {

        if (error.response.status === 503) {

          store.commit("error/SET_MAINTENANCE_CODE", true);

          redirect('/error');

        }

        else if (store.state.login.user_credential && error.response.status === 401) {

          let accessToken = Cookies.get('x-allpasal-access-token');

          let refreshToken = Cookies.get('x-allpasal-refresh-token');


          if (!accessToken) {

            if (refreshToken) {

              store.dispatch("login/refreshToken", {

                refresh_token: refreshToken

              }).catch(() => {

                localStorage.removeItem("login");

                localStorage.clear();

                Cookies.remove('x-allpasal-access-token');

                Cookies.remove('x-allpasal-refresh-token');

                store.commit("login/SET_ISLOGINED", false);

                store.commit("login/LOGOUT");

                redirect('/')

              });

            } else {

              localStorage.removeItem("login");

              Cookies.remove('x-allpasal-access-token');

              Cookies.remove('x-allpasal-refresh-token');

              localStorage.clear();

              store.commit("login/SET_ISLOGINED", false);

              store.commit("login/LOGOUT");

              redirect('/')

            }

          }

        }

        else if (error.response.status === 403 && error.response.data.data.user_log_out) {

          localStorage.removeItem("x-allpasal-access-token");

          localStorage.clear();

          store.commit("login/SET_ISLOGINED", false);

          store.commit("login/LOGOUT");

          redirect('/')

        }

        else if (error.response.status === 400 || error.response.status === 404) {

          redirect('/error');

        }

        else {

          store.commit("error/SET_MAINTENANCE_CODE", false);

        }

      } else if (error.response.code) {

        if (error.response.code === 503) {

          store.commit("error/SET_MAINTENANCE_CODE", true);

          redirect('/error');

        }

        else if (store.state.login.user_credential && error.response.code === 401) {

          let accessToken = Cookies.get('x-allpasal-access-token');

          let refreshToken = Cookies.get('x-allpasal-refresh-token');


          if (!accessToken) {

            if (refreshToken) {

              store.dispatch("login/refreshToken", {

                refresh_token: refreshToken

              }).catch(() => {

                localStorage.removeItem("login");

                localStorage.clear();

                Cookies.remove('x-allpasal-access-token');

                Cookies.remove('x-allpasal-refresh-token');

                store.commit("login/SET_ISLOGINED", false);

                store.commit("login/LOGOUT");

                redirect('/')

              });

            } else {

              localStorage.removeItem("login");

              Cookies.remove('x-allpasal-access-token');

              Cookies.remove('x-allpasal-refresh-token');

              localStorage.clear();

              store.commit("login/SET_ISLOGINED", false);

              store.commit("login/LOGOUT");

              redirect('/')

            }

          }

        }

        else if (error.response.code === 403 && error.response.data.data.user_log_out) {

          localStorage.removeItem("x-allpasal-access-token");

          localStorage.clear();

          store.commit("login/SET_ISLOGINED", false);

          store.commit("login/LOGOUT");

          redirect('/')

        }

        else if (error.response.code === 400 || error.response.code === 404) {

          redirect('/error');

        }

        else {

          store.commit("error/SET_MAINTENANCE_CODE", false);

        }

      }

    }

  })

}
